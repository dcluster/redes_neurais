import spektral as spk
from spektral.layers import GraphConv, GraphAttention
from tensorflow.keras import activations, initializers, regularizers, constraints
from tensorflow.keras.layers import Dense, Input, LSTM, concatenate, Layer, LSTMCell, RNN, Flatten, Conv2D
from tensorflow.keras.models import Model
import tensorflow as tf
from custom_lstm import CustomLSTM
from utils import corpus_to_dict, word_to_int, snapshot, adjacency_matrix, split_x, split_a
import numpy as np
import pandas as pd
from keras.utils import np_utils
import tensorflow.keras.backend as k

class WD_GCN():

    def __init__(self, n_vertices, lstm_units, channels):
        self.n_vertices = n_vertices
        self.lstm_units = lstm_units
        self.channels = channels

    def build(self):

        a = Input(shape=(10, 10))
        x = Input(shape=(10, 10))

        #joint = concatenate(inputs=[x, a])
        print("a hspae: ", a.shape)
        print("x hsape: ", x.shape)
        gc = GraphConv(channels=15, activation='relu', name="gcn")([x, a])
        print("gc shape: ", gc.shape, gc.shape[1], type(gc))
        gc_lstm = CustomLSTM(units=2, return_sequences=True)(gc)
        print("gc lstm shape: ", gc_lstm.shape)
        flatten = Flatten()(gc)
        print("flatten shape: ", flatten.shape)
        dense = Dense(15, activation="softmax")(flatten)
        print("dense shape: ", dense.shape)
        wd_gcn = Model(inputs=[a, x], outputs=[dense], name="wd_gcn")

        return wd_gcn

if __name__ =="__main__":

    adjacency_name = "/home/claudio/Documentos/datasets_redes_neurais/weeplaces/weeplace_friends.csv"
    df_friends = pd.read_csv(adjacency_name)
    corpus_vertices = pd.Series(df_friends['userid1'].unique().tolist() + df_friends['userid2'].unique().tolist()).unique().tolist()
    corpus_vertices_dict = corpus_to_dict(corpus_vertices)
    userid1 = word_to_int(corpus_vertices_dict, df_friends['userid1'].unique().tolist())
    userid2 = word_to_int(corpus_vertices_dict, df_friends['userid2'].unique().tolist())
    corpus_vertices_int = word_to_int(corpus_vertices_dict, corpus_vertices)
    n_snapshot = 10
    min_samples_per_user = 200
    n_graphs = int(min_samples_per_user / n_snapshot)
    checkins_name = "/home/claudio/Documentos/datasets_redes_neurais/weeplaces/weeplace_checkins.csv"
    df_checkins = pd.read_csv(checkins_name)
    df_checkins['datetime'] = pd.to_datetime(df_checkins['datetime'], infer_datetime_format=True)
    corpus_categories = df_checkins['category'].unique().tolist()
    corpus_categories_dict = corpus_to_dict(corpus_categories)
    df_checkins['category'] = pd.Series(word_to_int(corpus_categories_dict, df_checkins['category'].tolist()))
    df_checkins['userid'] = pd.Series(word_to_int(corpus_vertices_dict, df_checkins['userid'].tolist()))
    max_users = 10
    x, frequent_users = snapshot(df_checkins, n_snapshot, min_samples_per_user, max_users)
    print("n de grafos (x matriz): ", len(x), " vertices por grafo: ", len(x[0]), " colunas por grafo: ", len(x[0][0]))
    n_vertices = len(corpus_vertices)
    lstm_units = 4
    channels = 2
    a = adjacency_matrix(userid1, userid2, n_snapshot, min_samples_per_user, frequent_users)
    print("n de grafos (matriz de adjacencia): ", len(a), " vertices por grafo: ", len(a[0]), " colunas por grafo: ", len(a[0][0]))
    train_percentage = 0.8
    a_train, a_test = split_a(a, train_percentage)
    x_train, y_train, x_test, y_test = split_x(x, train_percentage)
    print("antes: ")
    print("a train shape: ", len(a_train), type(a_train), " vertices: ", len(a_train[0]), type(a_train[0]), " features: ", len(a_train[0][0]), type(a_train[0][0]))
    print("x train: ", len(x_train), type(x_train), " vertices: ", len(x_train[0]), type(x_train[0]), " features: ", len(x_train[0][0]), type(x_train[0][0]))
    print("y train: ", len(y_train), type(y_train), " vertices: ", len(y_train[0]), type(y_train[0]), " features: ", len(y_train[0][0]), type(y_train[0][0]))
    print("a test shape: ", len(a_test), type(a_test), " vertices: ", len(a_test[0]), type(a_test[0]), " features: ", len(a_test[0][0]), type(a_test[0][0]))
    print("x test: ", len(x_test), type(x_test), " vertices: ", len(x_test[0]), type(x_test[0]), " features: ", len(x_test[0][0]), type(x_test[0][0]))
    print("y test: ", len(y_test), type(y_test), " vertices: ", len(y_test[0]), type(y_test[0]), " features: ", len(y_test[0][0]), type(y_test[0][0]))
    model = WD_GCN(n_vertices, lstm_units, channels).build()

    model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=['accuracy'])
    hi = model.fit(x=[a_train, x_train], y=np_utils.to_categorical([j for j in range(15)], num_classes=15), batch_size=1, epochs=10)
    # , validation_data=([a, x_test], np.asarray([i for i in range(3)])),

    h = pd.DataFrame(hi.history)
    print("summary: ", model.summary())
