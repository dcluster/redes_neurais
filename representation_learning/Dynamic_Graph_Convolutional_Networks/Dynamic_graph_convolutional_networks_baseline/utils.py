import numpy as np
import pandas as pd
import copy

def adjacency_matrix(userid1, userid2, n_snapshot, min_samples_per_user, frequent_users):
    n_graphs = int(min_samples_per_user/n_snapshot)
    n_users = len(frequent_users)
    a = np.asarray([np.asarray([0 for v2 in frequent_users[:n_snapshot]]) for v in frequent_users])
    for i in range(len(frequent_users)):
        for j in range(n_snapshot):
            if userid1[i] >= n_users or userid2[j] >= n_snapshot:
                continue
            a[userid1[i]][userid2[j]] = a[userid1[i]][userid2[j]] + 1

    a_graphs = []
    for i in range(n_graphs):
        a_graphs.append(copy.copy(a))

    return np.asarray(a_graphs)

def corpus_to_dict(corpus):

    corpus_dict = {}
    count = 0
    for word in corpus:
        corpus_dict[word] = count
        count = count + 1

    return corpus_dict

def word_to_int(corpus_dict, words):

    int_list = []
    for word in words:
        int_list.append(corpus_dict[word])

    return int_list

def pre_processing_user_snapshot(df_user, n_snapshot, min_samples_per_user):
    # getting one visited location per day
    snap = df_user[['datetime', 'category']].drop_duplicates('datetime')
    snap = snap.head(min_samples_per_user)
    if snap.shape[0] < min_samples_per_user:
        print("erro")

    user_graphs = []
    user_snapshots = []

    for i in range(snap.shape[0]):
        user_snapshots.append(snap['category'].iloc[i])
        if len(user_snapshots) == n_snapshot:
            user_graphs.append(copy.copy(user_snapshots))
            user_snapshots = []

    return user_graphs

def snapshot(df_checkins, n_snapshot, min_samples_per_user, max_users):
    print("quantidade de categorias de locais: ", len(df_checkins['category'].unique().tolist()))
    df_checkins['datetime'] = df_checkins['datetime'].dt.date
    # one location per day
    quantidade = df_checkins[['userid', 'datetime']].drop_duplicates()
    quantidade = quantidade.groupby('userid').apply(lambda e: pd.Series([len(e['datetime'])]))
    quantidade = quantidade.rename(columns={"userid": "userid", 0: "quantidade"}).reset_index()
    frequent_users = quantidade.query("quantidade > " + str(min_samples_per_user))
    print("usuarios frequentes: ", frequent_users.shape)
    df_checkins = df_checkins.query("userid in " + str(frequent_users['userid'].tolist()[:max_users]))
    print("usuarios frequentes final: ", len(df_checkins['userid'].unique().tolist()))
    # generates a set of snapshots per user. Each snapshot is a list.
    df_checkins = df_checkins[['userid', 'datetime', 'category']].\
        groupby('userid').\
        apply(lambda e: pre_processing_user_snapshot(e, n_snapshot, min_samples_per_user))

    df_checkins = df_checkins.reset_index()
    df_checkins = df_checkins.rename(columns={"userid": "userid", 0: "snapshot"})
    n_graphs = int(min_samples_per_user/n_snapshot)
    df_checkins_snapshots = df_checkins['snapshot'].tolist()
    df_checkins_userid = df_checkins['userid'].tolist()
    x_graph = []
    for i in range(n_graphs):
        x_userid_snapshot_sample = []
        # for each user. each one is processed one time
        for j in range(df_checkins.shape[0]):
            # userid, snap1, snap2, ..., snapN
            x_userid_snapshot_sample.append(np.asarray(df_checkins_snapshots[j][i]))
            #print("userid snap: ", np.asarray([df_checkins_userid[j]] + df_checkins_snapshots[j][i]))

        x_graph.append(x_userid_snapshot_sample)
    return x_graph, df_checkins['userid'].unique().tolist()

def split_x(x, train_percentage):
    #x = x[:100]
    size = len(x)
    train_size = int(train_percentage*size)
    print("quantidade de grafos (matrix x) no treinamento: ", train_size)
    # train = x['snapshot'].apply(lambda e: e[:train_size])
    # test = x['snapshot'].apply(lambda e: e[train_size:])
    # x_train = train[:len(train)-1].tolist()
    # y_train = train[1:].tolist()
    # x_test = test[:len(train) - 1].tolist()
    # y_test = test[1:].tolist()
    train = x[:train_size]
    test = x[train_size:]
    # -1 porque é para prever a categoria os PoIs do próximo grafo/node prediction
    x_train = train[:len(train) - 1]
    y_train = train[1:]
    x_test = test[:len(test) - 1]
    y_test = test[1:]
    print("quantidade de grafos (matrix x) no treinamento: ", len(x_train), " y: ", len(y_train))
    print("quantidade de grafos (matrix x) no test: ", len(x_test), " y: ", len(y_test))
    return np.asarray(x_train), np.asarray(y_train), np.asarray(x_test), np.asarray(y_test)

def split_a(a, train_percentage):
    a = a[:len(a)-1]
    size = len(a)
    train_size = int(train_percentage * size)
    train = a[:train_size]
    test = a[train_size+1:]
    return train, test
