from tensorflow.keras import activations, initializers, regularizers, constraints
from tensorflow.keras.layers import Dense, Input, LSTM, concatenate, Layer, LSTMCell, RNN
from tensorflow.python.platform import tf_logging as logging
from tensorflow.python.keras.engine.input_spec import InputSpec
from tensorflow.python.framework import ops
from tensorflow.python.util.tf_export import keras_export
import tensorflow.keras.backend as k
import tensorflow as tf
import numpy as np

class CustomLSTM(LSTM):
    def __init__(self,
             units,
             activation='tanh',
             recurrent_activation='sigmoid',
             use_bias=True,
             kernel_initializer='glorot_uniform',
             recurrent_initializer='orthogonal',
             bias_initializer='zeros',
             unit_forget_bias=True,
             kernel_regularizer=None,
             recurrent_regularizer=None,
             bias_regularizer=None,
             activity_regularizer=None,
             kernel_constraint=None,
             recurrent_constraint=None,
             bias_constraint=None,
             dropout=0.,
             recurrent_dropout=0.,
             implementation=2,
             return_sequences=False,
             return_state=False,
             go_backwards=False,
             stateful=False,
             time_major=False,
             unroll=False,
             **kwargs):
        # if 'input_shape' not in kwargs and 'input_dim' in kwargs:
        #     kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        #super(CustomLSTM, self).__init__(units, return_sequences, **kwargs)
        super(CustomLSTM, self).__init__(
            units,
            activation=activation,
            recurrent_activation=recurrent_activation,
            use_bias=use_bias,
            kernel_initializer=kernel_initializer,
            recurrent_initializer=recurrent_initializer,
            bias_initializer=bias_initializer,
            unit_forget_bias=unit_forget_bias,
            kernel_regularizer=kernel_regularizer,
            recurrent_regularizer=recurrent_regularizer,
            bias_regularizer=bias_regularizer,
            activity_regularizer=activity_regularizer,
            kernel_constraint=kernel_constraint,
            recurrent_constraint=recurrent_constraint,
            bias_constraint=bias_constraint,
            dropout=dropout,
            recurrent_dropout=recurrent_dropout,
            implementation=implementation,
            return_sequences=return_sequences,
            return_state=return_state,
            go_backwards=go_backwards,
            stateful=stateful,
            time_major=time_major,
            unroll=unroll,
            **kwargs)

    def build(self, input_shape):
        print("Dimensao do build: ", (input_shape[0], 1, input_shape[2]))
        super(CustomLSTM, self).build((input_shape[0], 1, input_shape[2]))
        print("Dimensao: ", input_shape[0], " Dimensao 2: ", input_shape)
        self.layers = []
        for i in range(input_shape[1]):
            layer = LSTM(self.units, return_sequences=self.return_sequences)
            #layer.build((input_shape[0], 1, input_shape[2]))
            self.layers.append(layer)

    def call(self, inputs):
        split = tf.split(inputs, num_or_size_splits=inputs.shape[1], axis=1)
        super(CustomLSTM, self).call(inputs=split[0])
        weights = self.weights
        for i in range(len(weights)):
            weights[i] = k.eval(weights[i])
        weights = np.asarray(weights)
        output = self.layers[0](split[0])
        for i in range(1, len(self.layers)):
            current = self.layers[i](split[i])
            self.layers[i].set_weights(weights)
            output = concatenate([output, current])
        return output