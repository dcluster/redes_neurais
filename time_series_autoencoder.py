import numpy as np
import pandas as pd
import keras
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras.layers import RepeatVector
from keras.layers import TimeDistributed


def create_dataset(X, y, time_steps=1):

    Xs, ys = [], []

    for i in range(len(X) - time_steps):

        v = X.iloc[i:(i + time_steps)].values

        Xs.append(v)

        ys.append(y.iloc[i + time_steps])

    return np.array(Xs), np.array(ys)

def sp():
    df = pd.read_csv('spx.csv', parse_dates=['date'], index_col='date')
    train_size = int(len(df) * 0.95)
    test_size = len(df) - train_size
    train, test = df.iloc[0:train_size], df.iloc[train_size:len(df)]
    print("antes: ", train.shape, test.shape)

    scaler = StandardScaler()
    scaler = scaler.fit(train[['close']])
    train['close'] = scaler.transform(train[['close']])
    test['close'] = scaler.transform(test[['close']])
    TIME_STEPS = 30
    # reshape to [samples, time_steps, n_features]

    X_train, y_train = create_dataset(
        train[['close']],
        train.close,
        TIME_STEPS
    )

    X_test, y_test = create_dataset(
        test[['close']],
        test.close,
        TIME_STEPS
    )

    print(X_train.shape)

    model = keras.Sequential()
    model.add(keras.layers.LSTM(
        units=64,
        input_shape=(X_train.shape[1], X_train.shape[2])
    ))
    model.add(keras.layers.Dropout(rate=0.2))
    model.add(keras.layers.RepeatVector(n=X_train.shape[1]))
    model.add(keras.layers.LSTM(units=64, return_sequences=True))
    model.add(keras.layers.Dropout(rate=0.2))
    model.add(
        keras.layers.TimeDistributed(
            keras.layers.Dense(units=X_train.shape[2])
        )
    )
    model.compile(loss='mae', optimizer='adam')
    print("tamanhos: ", X_train.shape, y_train.shape)
    history = model.fit(
        X_train, y_train,
        epochs=10,
        batch_size=32,
        validation_split=0.1,
        shuffle=False
    )

    X_train_pred = model.predict(X_train)
    train_mae_loss = np.mean(np.abs(X_train_pred - X_train), axis=1)
    THRESHOLD = 0.65
    X_test_pred = model.predict(X_test)
    test_mae_loss = np.mean(np.abs(X_test_pred - X_test), axis=1)
    test_score_df = pd.DataFrame(index=test[TIME_STEPS:].index)

    test_score_df['loss'] = test_mae_loss
    test_score_df['threshold'] = THRESHOLD
    test_score_df['anomaly'] = test_score_df.loss > test_score_df.threshold
    test_score_df['close'] = test[TIME_STEPS:].close
    anomalies = test_score_df[test_score_df.anomaly == True]

# lstm autoencoder to recreate a timeseries

'''
A UDF to convert input data into 3-D
array as required for LSTM network.
'''

def temporalize(X, y, lookback):
    output_X = []
    output_y = []
    for i in range(len(X)-lookback-1):
        t = []
        for j in range(1,lookback+1):
            # Gather past records upto the lookback period
            t.append(X[[(i+j+1)], :])
        output_X.append(t)
        output_y.append(y[i+lookback+1])
    return output_X, output_y

if __name__ == "__main__":
    # define input timeseries
    timeseries = np.array([[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9],
                           [0.1 ** 3, 0.2 ** 3, 0.3 ** 3, 0.4 ** 3, 0.5 ** 3, 0.6 ** 3, 0.7 ** 3, 0.8 ** 3,
                            0.9 ** 3]]).transpose()

    timesteps = timeseries.shape[0]
    n_features = timeseries.shape[1]
    print(timeseries)
    timesteps = 3
    X, y = temporalize(X=timeseries, y=np.zeros(len(timeseries)), lookback=timesteps)

    n_features = 2
    X = np.array(X)
    X = X.reshape(X.shape[0], timesteps, n_features)
    print(X)
    # define model
    model = Sequential()
    model.add(LSTM(128, activation='relu', input_shape=(timesteps, n_features), return_sequences=True))
    model.add(LSTM(64, activation='relu', return_sequences=False))
    model.add(RepeatVector(timesteps))
    model.add(LSTM(64, activation='relu', return_sequences=True))
    model.add(LSTM(128, activation='relu', return_sequences=True))
    model.add(TimeDistributed(Dense(n_features)))
    model.compile(optimizer='adam', loss='mse')
    model.summary()
    # fit model
    model.fit(X, X, epochs=300, batch_size=5, verbose=0)
    # demonstrate reconstruction
    yhat = model.predict(X, verbose=0)
    print('---Predicted---')
    print(np.round(yhat, 3))
    print('---Actual---')
    print(np.round(X, 3))

